const songs = [
    {
        src: "https://archive.org/download/78_mambo-no.-5_perez-prado-and-his-orchestra-d.-perez-prado_gbia0009774b/Mambo%20No.%205%20-%20Perez%20Prado%20and%20his%20Orchestra-restored.mp3",
        title: "Mambo No. 5",
        artist: "Perez Prado and his Orchestra",
        album: "Mambo No. 5",
        artwork: [
            {
                src: "https://archive.org/download/78_mambo-no.-5_perez-prado-and-his-orchestra-d.-perez-prado_gbia0009774b/78_mambo-no.-5_perez-prado-and-his-orchestra-d.-perez-prado_gbia0009774b.jpg",
                sizes: "2182x2182",
                type: "image/jpeg"
            },
            {
                src: "https://ia800803.us.archive.org/22/items/78_mambo-no.-5_perez-prado-and-his-orchestra-d.-perez-prado_gbia0009774b/78_mambo-no.-5_perez-prado-and-his-orchestra-d.-perez-prado_gbia0009774b_itemimage.jpg",
                sizes: "1680x1680",
                type: "image/jpeg"
            },
            {
                src: "https://ia800803.us.archive.org/22/items/78_mambo-no.-5_perez-prado-and-his-orchestra-d.-perez-prado_gbia0009774b/78_mambo-no.-5_perez-prado-and-his-orchestra-d.-perez-prado_gbia0009774b_thumb.jpg",
                sizes: "192x192",
                type: "image/jpeg"
            }
        ]
    },
    {
        src: "https://archive.org/download/PeterAndTheWolf_753/PeterAndTheWolf_01.mp3",
        title: "Peter and the Wolf",
        artist: "Prokofiev",
        album: "Peter and the Wolf",
        artwork: [
            {
                src: "https://ia802802.us.archive.org/26/items/PeterAndTheWolf_753/DSCN6175.JPG",
                sizes: "2796x2872",
                type: "image/jpeg"
            },
            {
                src: "https://ia802802.us.archive.org/26/items/PeterAndTheWolf_753/DSCN6175_thumb.jpg",
                sizes: "187x192",
                type: "image/jpeg"
            }
        ]
    },
    {
        src: "https://archive.org/download/78_mexican-childrens-games_david-pryor-and-henry-lundy-and-group-at-nassau-bahamas_gbia8002358/01%20-%201.%20DIG%20MY%20GRAVE%20-%20David%20Pryor%20and%20Henry%20Lundy%2C%20and%20group%20at%20Nassau%2C%20Bahamas.mp3",
        title: "Dig My Grave",
        artist: "David Pryor and Henry Lundy",
        album: "Archive of American Folk Song",
        artwork: [
            {
                src: "https://archive.org/download/78_mexican-childrens-games_david-pryor-and-henry-lundy-and-group-at-nassau-bahamas_gbia8002358/78_mexican-childrens-games_david-pryor-and-henry-lundy-and-group-at-nassau-bahamas_gbia8002358.jpg",
                sizes: "2263x2263",
                type: "image/jpeg"
            },
            {
                src: "https://ia801701.us.archive.org/16/items/78_mexican-childrens-games_david-pryor-and-henry-lundy-and-group-at-nassau-bahamas_gbia8002358/78_mexican-childrens-games_david-pryor-and-henry-lundy-and-group-at-nassau-bahamas_gbia8002358_thumb.jpg",
                sizes: "192x192",
                type: "image/jpeg"
            }
        ]
    }
];
