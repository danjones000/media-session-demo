/* global MediaMetadata */

(function () {

    function MediaSessionControl(player, tracks, config) {
        // @todo ensure player is an audio/video element
        // @todo ensure tracks is a valid array

        config = config || {};

        this.player = player;
        this.tracks = tracks;
        this.isStopped = true;
        this.loop = config.loop || false;

        this.initMediaSession();
        this.initMediaEvents();
        this.playTrack(0);
    }

    MediaSessionControl.prototype.handlers = [
        'play',
        'pause',
        'previoustrack',
        'nexttrack',
        'seekbackward',
        'seekforward',
        'seekto',
        'stop',
    ];

    MediaSessionControl.prototype.initMediaSession = function () {
        if (!('mediaSession' in navigator)) {
            return;
        }

        for (const idx in this.handlers) {
            const handler = this.handlers[idx];
            try {
                navigator.mediaSession.setActionHandler(handler, (details) => this._handle(details));
            } catch (error) {
                console.error(`mediaSession does not support ${handler}`, error);
            }
        }
    };

    MediaSessionControl.prototype.initMediaEvents = function () {
        this.player.addEventListener('ended', () => this.nexttrack());
    };

    MediaSessionControl.prototype._handle = function (details) {
        const action = details.action;
        console.info(action, details);

        this[action](details);
    };

    MediaSessionControl.prototype.playTrack = function (idx) {
        let toPlay = idx;
        while (toPlay < 0) {
            toPlay += this.tracks.length;
        }
        toPlay = toPlay % this.tracks.length;
        const current = this.tracks[toPlay];

        console.log(`Playing ${current.title}: ${current.src}`);

        this.player.src = current.src;

        try {
            this.player.play();
            this.isStopped = false;
        } catch (error) {
            // browser is currently blocking auto-playing
        }

        if (!('mediaSession' in navigator)) {
            return;
        }

        navigator.mediaSession.metadata = new MediaMetadata(current);
    };

    MediaSessionControl.prototype.getCurrentIdx = function () {
        let current = this.player.src;

        return this.tracks.reduce((carry, curr, idx) => curr.src == current ? idx : carry, 0);
    };

    MediaSessionControl.prototype.play = function () {
        if (this.isStopped) {
            return this.playTrack(this.getCurrentIdx());
        }

        try {
            this.player.play();
            this.isStopped = false;
        } catch (error) {
            // browser is currently blocking auto-playing
            console.error(error);
        }

        // returning null to make IDE happy
        return null;
    };

    MediaSessionControl.prototype.pause = function () {
        this.player.pause();
    };

    MediaSessionControl.prototype.stop = function () {
        this.player.pause();
        this.player.currentTime = 0;
        this.isStopped = true;

        if (!('mediaSession' in navigator)) {
            return;
        }

        navigator.mediaSession.metadata = null;
    };

    MediaSessionControl.prototype.seekbackward = function (details) {
        console.info('seekbackward not yet supported', details);
    };

    MediaSessionControl.prototype.seekforward = function (details) {
        console.info('seekforward not yet supported', details);
    };

    MediaSessionControl.prototype.seekto = function (details) {
        console.info('seekto not yet supported', details);
    };

    MediaSessionControl.prototype.nexttrack = function () {
        const track = this.getCurrentIdx() + 1;
        if (track == this.tracks.length && !this.loop) {
            return this.stop();
        }

        return this.playTrack(track);
    };

    MediaSessionControl.prototype.previoustrack = function () {
        const track = this.getCurrentIdx() - 1;
        if (track < 0 && !this.loop) {
            return this.stop();
        }

        return this.playTrack(track);
    };

    window.MediaSessionControl = MediaSessionControl;
})();
